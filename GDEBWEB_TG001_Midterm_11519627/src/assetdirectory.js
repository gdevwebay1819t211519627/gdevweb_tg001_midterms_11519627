export let AssetDirectory = {
	"load": [
		"assets/fonts/andayani.eot",
		"assets/fonts/andayani.svg",
		"assets/fonts/andayani.ttf",
		"assets/fonts/andayani.woff",
		"assets/fonts/andayani.woff2",
		"assets/images/ball-blue.png",
		"assets/images/ball-green.png",
		"assets/images/ball-red.png",
		"assets/images/ball-yellow.png",
		"assets/images/empty.png",
		"assets/images/game-case.png",
		"assets/images/gray.png",
		"assets/images/proceed-btn.png",
		"assets/images/Restart.png",
		"assets/images/white.png",
		"assets/images/wrong.png"
	],
	"audio": []
};