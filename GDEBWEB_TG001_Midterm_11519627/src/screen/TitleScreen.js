import { Container, Text, TEXT_GRADIENT, Graphics, utils} from "pixi.js";

class TitleScreen extends Container{
    constructor(){
        super();


        ////////////// +++++++ SHAPES ++++++++ ////////////////
console.log("OY ANO ANA")


    var TextureCache = PIXI.utils.TextureCache;

    var texture_gameCase = PIXI.Texture.fromImage("assets/images/game-case.png")   
    var gameCase = new PIXI.Sprite(texture_gameCase);

    var rowsCompleted = 0;

    gameCase.x =100;

    //#region ***Image URLS***
    var holderTextureURL = "assets/images/empty.png";
    var proceedBtnURL = "assets/images/proceed-btn.png"
    var blueURL = "assets/images/ball-blue.png";
    var redURL = "assets/images/ball-red.png";
    var greenURL = "assets/images/ball-green.png";
    var yellowURL = "assets/images/ball-yellow.png";
    var grayURL = "assets/images/gray.png";
    var whiteURL = "assets/images/white.png";
    var wrongURL = "assets/images/wrong.png";
    var restartURL = "assets/images/Restart.png";
    //#endregion

    var color_index = 0;
    var id;
    
    //#region ***Array of Colors***
    var colors = (c_blue, c_red, c_green, c_yellow) =>
    {
        var colorArray = [c_blue, c_red, c_green, c_yellow];

        return colorArray;

    }

    var ball_colors = colors(blueURL, redURL, greenURL, yellowURL);

    //#endregion


    //#region ***Ball placeholders***
    var empties = ( texture, x=0, y=0, colors = [], bool, id) =>
    {
        var button = PIXI.Sprite.fromImage(texture) 
        button.name = id;
        button.interactive = bool
        button.x = x;
        button.y = y;
        button.buttonMode = bool;
        //console.log("X: " + button.x + " Y " + button.y );
        
       
        button.on('pointerdown', ()=>
        {
            
            if(color_index < colors.length)
            {
                var newbutton = PIXI.Sprite.fromImage(colors[color_index]);
                newbutton.position = button.position;
                this.addChild(newbutton);

                
                
                if(color_index == 0)
                {
                    id = "BLUE";
                    button.name  =id;
                }
                if(color_index == 1)
                {
                    id = "RED";
                    button.name  = id;
                }     
                if(color_index == 2)
                {
                    id = "GREEN";
                    button.name  = id;
                }   
                if(color_index == 3)
                {
                    id = "YELLOW";
                    button.name  = id;
                }           
                color_index += 1;
                if(color_index >= colors.length)
                {
                    color_index = 0;
                }
                
                
            } 
            
        
        });
        return button;
    }

    //#endregion

    //#region ***Given Colors***
    var random1 = Math.floor(Math.random() * 4);
    var random2 = Math.floor(Math.random() * 4);
    var random3 = Math.floor(Math.random() * 4);
    var random4 = Math.floor(Math.random() * 4);

    var setID = function(number, objID)
    {
        var objID;
        if(number == 0)
        {
            objID = "BLUE"
        }
        if(number == 1)
        {
            objID = "RED"
        }
        if(number == 2)
        {
            objID = "GREEN"
        }
        if(number == 3)
        {
            objID = "YELLOW"
        }    

        return objID;
    }

    var _IDgiven = "";

    var columnOneID = setID(random1, _IDgiven );
    var columnTwoID = setID(random2, _IDgiven );
    var columnThreeID = setID(random3, _IDgiven );
    var columnFourID = setID(random4, _IDgiven );

    var _firstColumn = empties( ball_colors[random1], 750, 32, ball_colors, false, columnOneID);
    var _secondColumn = empties( ball_colors[random2], _firstColumn.x+68, 32, ball_colors, false, columnTwoID);
    var _thirdColumn = empties( ball_colors[random3], _secondColumn.x+68, 32, ball_colors, false,columnThreeID);
    var _fourthColumn = empties( ball_colors[random4], _thirdColumn.x+68, 32, ball_colors, false, columnFourID);

  
    var givenBalls_ = (first, second, third, fourth)=>
    {
        var arrayGiven = [first, second, third, fourth];

        
        
        return arrayGiven;

    }

    var given = givenBalls_ (_firstColumn, _secondColumn, _thirdColumn, _fourthColumn);

    //#endregion


    //#region ***ROWS***
    var Rows_ = (url, x, y, offsetX, colorurl, bool) =>
    {
        //#region TEST PARAMS
        // orig param = first, second, third, fourth, rowPosY, columnOnePosX, offsetX
        //y = 582
        //x = 164
        //offset = 68
        //col = new empties(url, x, y, offsetX, offsetY, colorurl)
        //#endregion

        var first = new empties(url, x, y, colorurl, bool)
        var second = new empties(url, x, y, colorurl, bool)
        var third = new empties(url, x, y, colorurl, bool)
        var fourth = new empties(url, x, y, colorurl, bool)

        var rowAarray_ = [first, second, third, fourth];
      
        rowAarray_[0].x = x;
        rowAarray_[0].y = y;

        rowAarray_[1].x = x+offsetX;
        rowAarray_[1].y = y;
        
        rowAarray_[2].x = rowAarray_[1].x+offsetX;
        rowAarray_[2].y = y;

        rowAarray_[3].x= rowAarray_[2].x+offsetX;
        rowAarray_[3].y = y;

        return  rowAarray_;
    }

    var rowOne_ = Rows_(holderTextureURL, 164, 582, 68,ball_colors, true);
    var rowTwo_ = Rows_(holderTextureURL, 164, 442,  68,ball_colors, true);
    var rowThree_ = Rows_(holderTextureURL, 167, 304,  68,ball_colors, true);
    var rowFour_ = Rows_(holderTextureURL, 164, 172,  68,ball_colors, true);
    var rowFive_ = Rows_(holderTextureURL,  169,32, 68,ball_colors, true);

    var selectedBalls_ = (first, second, third, fourth)=>
    {
        var arraySelected = [first, second, third, fourth];

       
        return arraySelected;

    }

    var selected1 = selectedBalls_ (rowOne_[0], rowOne_[1], rowOne_[2], rowOne_[3]);
    var selected2 = selectedBalls_ (rowTwo_[0], rowTwo_[1], rowTwo_[2], rowTwo_[3]);
    var selected3 = selectedBalls_ (rowThree_[0], rowThree_[1], rowThree_[2],rowThree_[3]);
    var selected4 = selectedBalls_ (rowFour_[0], rowFour_[1], rowFour_[2], rowFour_[3]);
    var selected5 = selectedBalls_ (rowFive_[0], rowFive_[1], rowFive_[2],rowFive_[3]);

    //#endregion

 
    console.log(given[0].name + " " + given[1].name);
    console.log(given[2].name + " " + given[3].name);

    var results = (url, x, y) =>
    {
        var columnResult = PIXI.Sprite.fromImage(url);
        columnResult.x =x;
        columnResult.y =y;

        return columnResult;
    }


    var checkBalls = function( given_ = [], selected_ = [])
    {   
       var id = [];
       var notExisting = [];
       
       for(var i = 0; i < given_.length; i++)
       {
           //match = false;
           var containing = given_.includes(selected_[i].name);
           var checkSelectionName = selected_[i].name; 
           
           if(checkSelectionName == given_[i].name)
           {
               console.log("TAMA KULAY, TAMA POS");
               id.push(whiteURL);
               
               continue;
           }
           if(checkSelectionName != given_[i].name)
           { 
              
               for(var j = 0; j < given_.length; j++)
               {
                   if(selected_[i].name == given_[j].name )
                   {
                       console.log("TAMA KULAY, MALI POS")
                       id.push(grayURL);
                      
                       break;
                   }
                   if(selected_[i].name != given_[j].name)
                   {
                       id.push(wrongURL);
                       break;
                   }
                   
                  console.log("END: " + j)
               }
           }
           //console.log("END NG FIRST FOR");
       }
       return id;

    }


    //#region ***CHECK BUTTON***
    var resultURL;
    var proceed_buttons = (url, x, y, bool =true) =>
    {
        var button = PIXI.Sprite.fromImage(url) 
        button.interactive = bool;
        button.x = x;
        button.y = y;
        button.buttonMode = bool;

        

        return button;

    } 

    var proceedButton1 = new proceed_buttons(proceedBtnURL, 500, 582, resultURL);
    proceedButton1.on('pointerdown', ()=>
    {
        rowsCompleted += 1;
        var check = checkBalls(given, selected1);
        console.log(check);
        var row1_colScore1= proceed_buttons(check[0], 500, 582, false)
        var row1_colScore2 = proceed_buttons(check[1], 600, 582, false)
        var row1_colScore3 = proceed_buttons(check[2], 500, 642, false)
        var row1_colScore4 = proceed_buttons(check[3], 600, 642, false)
        
        if(rowsCompleted == 1)
        {
            this.addChild(proceedButton2);
            this.addChild(row1_colScore1);
            this.addChild(row1_colScore2);
            this.addChild(row1_colScore3);
            this.addChild(row1_colScore4);
        }
            
        
    });
    var proceedButton2 = new proceed_buttons(proceedBtnURL, 500, 442);
    proceedButton2.on('pointerdown', ()=>
    {
        if(rowsCompleted < 4)
        {
            rowsCompleted += 1;
            var check = checkBalls(given, selected2);
            this.removeChild(proceedButton2);

            var row2_colScore1= proceed_buttons(check[0], 500, 442, false)
            var row2_colScore2 = proceed_buttons(check[1], 600, 442, false)
            var row2_colScore3 = proceed_buttons(check[2], 500, 502, false)
            var row2_colScore4 = proceed_buttons(check[3], 600, 502, false)


       
            if(rowsCompleted == 2)
            {
                this.addChild(proceedButton3);
                this.addChild(row2_colScore1);
                this.addChild(row2_colScore2);
                this.addChild(row2_colScore3);
                this.addChild(row2_colScore4);
            }
            

        }
    });
    var proceedButton3 = new proceed_buttons(proceedBtnURL, 500, 304);
    proceedButton3.on('pointerdown', ()=>
    {
        if(rowsCompleted < 4)
        {
            rowsCompleted += 1;
            var check = checkBalls(given, selected3);
            this.removeChild(proceedButton3);

            var row3_colScore1= proceed_buttons(check[0], 500, 302, false)
            var row3_colScore2 = proceed_buttons(check[1], 600, 302, false)
            var row3_colScore3 = proceed_buttons(check[2], 500, 362, false)
            var row3_colScore4 = proceed_buttons(check[3], 600, 362, false)

            if(rowsCompleted == 3)
            {
                 this.addChild(proceedButton4);
                 this.addChild(row3_colScore1);
                this.addChild(row3_colScore2);
                this.addChild(row3_colScore3);
                this.addChild(row3_colScore4);
            }
            

        }
    });
    var proceedButton4 = new proceed_buttons(proceedBtnURL, 500, 172);
    proceedButton4.on('pointerdown', ()=>
    {
        if(rowsCompleted < 4)
        {
            rowsCompleted += 1;
            var check = checkBalls(given, selected4);
            this.removeChild(proceedButton4);

            var row4_colScore1= proceed_buttons(check[0], 500, 172, false)
            var row4_colScore2 = proceed_buttons(check[1], 600, 172, false)
            var row4_colScore3 = proceed_buttons(check[2], 500, 232, false)
            var row4_colScore4 = proceed_buttons(check[3], 600, 232, false)

            if(rowsCompleted == 4)
            {
                this.addChild(proceedButton5);
                this.addChild(row4_colScore1);
                this.addChild(row4_colScore2);
                this.addChild(row4_colScore3);
                this.addChild(row4_colScore4);
            }

        }
    });
    var proceedButton5 = new proceed_buttons(proceedBtnURL, 500, 32);
    proceedButton5.on('pointerdown', ()=>
    {
        if(rowsCompleted <= 4)
        {
            rowsCompleted += 1;
            var check = checkBalls(given, selected5);
            this.removeChild(proceedButton5);

            var row5_colScore1= proceed_buttons(check[0], 500, 32, false)
            var row5_colScore2 = proceed_buttons(check[1], 600, 32, false)
            var row5_colScore3 = proceed_buttons(check[2], 500, 92, false)
            var row5_colScore4 = proceed_buttons(check[3], 600, 92, false)

            this.addChild(_firstColumn, _secondColumn, _thirdColumn, _fourthColumn);

            this.addChild(row5_colScore1);
            this.addChild(row5_colScore2);
            this.addChild(row5_colScore3);
            this.addChild(row5_colScore4);

            this.addChild(proceedButton6);
        
        }
    });

    var proceedButton6 = new proceed_buttons(restartURL, 850, 90);
    proceedButton6.on('pointerdown', ()=>
    {
        location.reload();
    });

    //#endregion

    
    this.addChild(gameCase); 
    this.addChild(proceedButton1);
    
    this.addChild(rowOne_[0], rowOne_[1], rowOne_[2], rowOne_[3]);
    this.addChild(rowTwo_[0], rowTwo_[1], rowTwo_[2], rowTwo_[3]);
    this.addChild(rowThree_[0], rowThree_[1], rowThree_[2], rowThree_[3]);
    this.addChild(rowFour_[0], rowFour_[1], rowFour_[2], rowFour_[3]);
    this.addChild(rowFive_[0], rowFive_[1], rowFive_[2], rowFive_[3]);

    //this.addChild(_firstColumn, _secondColumn, _thirdColumn, _fourthColumn);

    }
}

export default TitleScreen;