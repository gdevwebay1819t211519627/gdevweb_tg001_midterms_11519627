'use strict';

var chalk = require('chalk')
var merge = require('merge')
var webpack = require('webpack')
var ora = require('ora')

var assetBuilder = require('../assetbuild.js');
assetBuilder.generateAssetList();

var webpackConfigBuild = require('./webpack.config.build.js');
var webpackConfigProduction = require('./webpack.config.prod.js');
var webpackBase = require('./webpack.config.base.js');

var webpackCompact;
var environment = null;
if (process.argv.length >= 2) {
	environment = process.argv[2];
}

console.log(chalk.cyan("ENVIRONMENT", environment));
var spinner = ora('building ...')
spinner.start()

if (environment) {
	if (environment === 'production') {
		webpackCompact = merge(webpackBase, webpackConfigProduction);
	} else if (environment == 'development') {
		webpackCompact = merge(webpackBase, webpackConfigBuild);
	}
	console.log(webpackCompact)
	webpack(webpackCompact, function (err, stats) {
		spinner.stop()
		console.log(stats.toString({
			colors: true,
			modules: true,
			children: false,
			chunks: false,
			chunkModules: false
		}));
		if (err || stats.hasErrors()) {
			console.error(err);
			throw err;
		}
		console.log(chalk.cyan('  Build complete.\n'))
	});

}
