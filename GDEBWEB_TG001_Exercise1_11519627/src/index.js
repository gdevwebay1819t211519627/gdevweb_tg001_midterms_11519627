import { AssetDirectory } from './assetdirectory.js';
import { Application } from 'pixi.js';
import Main from './Main.js';

var Config = require('Config');


let canvas = document.getElementById('game-canvas');
let pixiapp = new Application({
    view: canvas,
    width: 2000,
    height: 1000
})


document.body.style.margin = "0px";
document.body.style.overflow = "hidden";

/*****************************************
 ************* ENTRY POINT ***************
 *****************************************/
function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    Main.start();
});

export {pixiapp as App};