export let AssetDirectory = {
	"load": [
		"assets/fonts/andayani.eot",
		"assets/fonts/andayani.svg",
		"assets/fonts/andayani.ttf",
		"assets/fonts/andayani.woff",
		"assets/fonts/andayani.woff2"
	],
	"audio": []
};