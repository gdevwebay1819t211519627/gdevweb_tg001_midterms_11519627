import { Container, Text, TEXT_GRADIENT, Graphics } from "pixi.js";

class TitleScreen extends Container{
    constructor(){
        super();

      var consoleText = new Text("Exercise1 in Console ", {fill: "0x00ffff", fontSize: 28, });
      consoleText.x = 30;
      consoleText.y = 100;

      console.log("Exercise1");
      
      // ============= #1 =============
      var largestArr = function(int1, int2, int3, int4, largestElem)
      {
          var array = [int1, int2, int3, int4]
          largestElem = 0;

          for (var i = 0; i<= largestElem; i++)
          {
              if(array[i] > largestElem)
              {
                largestElem = array[i];
              }
          }

          return console.log("1. Find the highest numbest in an array: [" + array + "] - Highest is " + largestElem);

      }

      // ============= #2 =============
      var sumArr = function(int1, int2, int3, int4) 
      {
          var array = [int1, int2, int3, int4];
          var total = 0;

          for(var i = 0; i< array.length; i++)
          {
              total += array[i];
          }

          return console.log("2. Find the sum of all element in an array: [" + array + "], Sum of the array is - " + total)

      }

      // ============= #3 =============
      var avgArr = function(int1, int2, int3, int4, int5, int6)
      {
          var array = [int1, int2, int3, int4, int5, int6]
          var average = 0;

          for(var i = 0; i < array.length; i++)
          {
              average += array[i];
          }

          return console.log("3. Find the average of given elements/numbers: The average of " + array + " is = " + average/array.length)
      }

      // ============= #4 =============
      var radConvert = function(int1, pie, degree)
      {
            pie = 3.14;
            degree = 180;

            var radian = int1 * pie/degree;

            return console.log("4. Convert a number from degrees to radians. " + int1 + " degrees is = " + radian + " radians ");
      }

      // ============= #5 =============
      var clamping = function(int1, min, max)
      {
          var unclamp = int1;

          if(int1 <= min)
          {
              int1 = min;
          }
          else if (int1 >= max)
          {
              int1 = max;
          }

          return console.log("5. A number that clamps to minimum and maximum. Unclamped number is = " + unclamp + ", Minimum = " + min + 
           ", Maximum = " + max + "\n" + "Clamped number = " + int1);
      }

      // ============= #6 =============
      var distance = function(x1, x2, y1, y2)
      {
          var distance = Math.sqrt((x2 - x1)*(x2-x1) + (y2 -y1) * (y2 -y1));

          return console.log("6. Get the Euclidean distance of the two points." + "\n" + "Point #1 = (" + 
          x1 + ", " +y1+ "), Point #2 = (" + x2 + ", " + y2 + "), The distance is = "  + distance)
      }

      // ============= #7 =============
      var sorting = function(int1, int2, int3, int4, int5)
      {
          var array = [int1, int2, int3, int4, int5]
          var unsorted = array;

          function sortNumber(a,b) 
          {
             return a - b;
          }

          return console.log("7. Sorting of number elements in an array (Lowest to Highest). Unsorted = [" + unsorted + "], Sorted = [" + array.sort(sortNumber) + "]");
      }

      // ============= #8 =============
      var object1 = {
          _x : 0,
          _y : 0,
          _Magnitude : 0
      }

      // ============= #9 =============
      var Person = (firstName, lastName)=>
      {
          this.firstName = firstName;
          this.lastName = lastName;

          this.getFullName = function()
          {
              var fullName = this.firstName + " " + this.lastName;

              return console.log("9. Create a class that has Firstname, Last name, and has getFullName function: " + "\n" 
              + "Fullname : " + fullName + "\n" + "Lastname: " + this.lastName + "\n" + "Firstname: " + this.firstName)
          }
          this.getFullName();
      }


      this.addChild(consoleText);

      var array1 = largestArr(12, 32, 55, 27);
      var array2 = sumArr(4, 10, 9, 5);
      var array3 = avgArr(5, 18, 12, 10, 25, 15);
      var degTorad = radConvert(90);
      var clamp = clamping(999699, 200, 350);
      var eucDistance = distance(2, -2, -1, 2);
      var sort = sorting(52, 25, 79, 102, 64);

      var createObj = new Object();
      createObj._x = 52;
      createObj._y = 25;
      createObj._Magnitude = createObj._x + createObj._y;

      console.log("8. Create an object with the x, y, and the length of the point: " + createObj);

      var myself = new Person("Ryu Elijah", "Elquiero");
      
        
    }
}

export default TitleScreen;