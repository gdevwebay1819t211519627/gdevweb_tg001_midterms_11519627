/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/andayani.eot\", \"assets/fonts/andayani.svg\", \"assets/fonts/andayani.ttf\", \"assets/fonts/andayani.woff\", \"assets/fonts/andayani.woff2\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS5lb3RcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS5zdmdcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS50dGZcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS53b2ZmXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvYW5kYXlhbmkud29mZjJcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtdXG59OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBT0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: 2000,\n    height: 1000\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiAyMDAwLFxyXG4gICAgaGVpZ2h0OiAxMDAwXHJcbn0pXHJcblxyXG5cclxuZG9jdW1lbnQuYm9keS5zdHlsZS5tYXJnaW4gPSBcIjBweFwiO1xyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gXCJoaWRkZW5cIjtcclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKiBFTlRSWSBQT0lOVCAqKioqKioqKioqKioqKipcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5mdW5jdGlvbiByZWFkeShmbikge1xyXG4gICAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT0gJ2xvYWRpbmcnKSB7XHJcbiAgICAgICAgZm4oKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZuKTtcclxuICAgIH1cclxufVxyXG5cclxucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgTWFpbi5zdGFydCgpO1xyXG59KTtcclxuXHJcbmV4cG9ydCB7cGl4aWFwcCBhcyBBcHB9OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n      value: true\n});\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n      _inherits(TitleScreen, _Container);\n\n      function TitleScreen() {\n            _classCallCheck(this, TitleScreen);\n\n            var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n            var consoleText = new _pixi.Text(\"Exercise1 in Console \", { fill: \"0x00ffff\", fontSize: 28 });\n            consoleText.x = 30;\n            consoleText.y = 100;\n\n            console.log(\"Exercise1\");\n\n            // ============= #1 =============\n            var largestArr = function largestArr(int1, int2, int3, int4, largestElem) {\n                  var array = [int1, int2, int3, int4];\n                  largestElem = 0;\n\n                  for (var i = 0; i <= largestElem; i++) {\n                        if (array[i] > largestElem) {\n                              largestElem = array[i];\n                        }\n                  }\n\n                  return console.log(\"1. Find the highest numbest in an array: [\" + array + \"] - Highest is \" + largestElem);\n            };\n\n            // ============= #2 =============\n            var sumArr = function sumArr(int1, int2, int3, int4) {\n                  var array = [int1, int2, int3, int4];\n                  var total = 0;\n\n                  for (var i = 0; i < array.length; i++) {\n                        total += array[i];\n                  }\n\n                  return console.log(\"2. Find the sum of all element in an array: [\" + array + \"], Sum of the array is - \" + total);\n            };\n\n            // ============= #3 =============\n            var avgArr = function avgArr(int1, int2, int3, int4, int5, int6) {\n                  var array = [int1, int2, int3, int4, int5, int6];\n                  var average = 0;\n\n                  for (var i = 0; i < array.length; i++) {\n                        average += array[i];\n                  }\n\n                  return console.log(\"3. Find the average of given elements/numbers: The average of \" + array + \" is = \" + average / array.length);\n            };\n\n            // ============= #4 =============\n            var radConvert = function radConvert(int1, pie, degree) {\n                  pie = 3.14;\n                  degree = 180;\n\n                  var radian = int1 * pie / degree;\n\n                  return console.log(\"4. Convert a number from degrees to radians. \" + int1 + \" degrees is = \" + radian + \" radians \");\n            };\n\n            // ============= #5 =============\n            var clamping = function clamping(int1, min, max) {\n                  var unclamp = int1;\n\n                  if (int1 <= min) {\n                        int1 = min;\n                  } else if (int1 >= max) {\n                        int1 = max;\n                  }\n\n                  return console.log(\"5. A number that clamps to minimum and maximum. Unclamped number is = \" + unclamp + \", Minimum = \" + min + \", Maximum = \" + max + \"\\n\" + \"Clamped number = \" + int1);\n            };\n\n            // ============= #6 =============\n            var distance = function distance(x1, x2, y1, y2) {\n                  var distance = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));\n\n                  return console.log(\"6. Get the Euclidean distance of the two points.\" + \"\\n\" + \"Point #1 = (\" + x1 + \", \" + y1 + \"), Point #2 = (\" + x2 + \", \" + y2 + \"), The distance is = \" + distance);\n            };\n\n            // ============= #7 =============\n            var sorting = function sorting(int1, int2, int3, int4, int5) {\n                  var array = [int1, int2, int3, int4, int5];\n                  var unsorted = array;\n\n                  function sortNumber(a, b) {\n                        return a - b;\n                  }\n\n                  return console.log(\"7. Sorting of number elements in an array (Lowest to Highest). Unsorted = [\" + unsorted + \"], Sorted = [\" + array.sort(sortNumber) + \"]\");\n            };\n\n            // ============= #8 =============\n            var object1 = {\n                  _x: 0,\n                  _y: 0,\n                  _Magnitude: 0\n\n                  // ============= #9 =============\n            };var Person = function Person(firstName, lastName) {\n                  _this.firstName = firstName;\n                  _this.lastName = lastName;\n\n                  _this.getFullName = function () {\n                        var fullName = this.firstName + \" \" + this.lastName;\n\n                        return console.log(\"9. Create a class that has Firstname, Last name, and has getFullName function: \" + \"\\n\" + \"Fullname : \" + fullName + \"\\n\" + \"Lastname: \" + this.lastName + \"\\n\" + \"Firstname: \" + this.firstName);\n                  };\n                  _this.getFullName();\n            };\n\n            _this.addChild(consoleText);\n\n            var array1 = largestArr(12, 32, 55, 27);\n            var array2 = sumArr(4, 10, 9, 5);\n            var array3 = avgArr(5, 18, 12, 10, 25, 15);\n            var degTorad = radConvert(90);\n            var clamp = clamping(999699, 200, 350);\n            var eucDistance = distance(2, -2, -1, 2);\n            var sort = sorting(52, 25, 79, 102, 64);\n\n            var createObj = new Object();\n            createObj._x = 52;\n            createObj._y = 25;\n            createObj._Magnitude = createObj._x + createObj._y;\n\n            console.log(\"8. Create an object with the x, y, and the length of the point: \" + createObj);\n\n            var myself = new Person(\"Ryu Elijah\", \"Elquiero\");\n\n            return _this;\n      }\n\n      return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsIFRFWFRfR1JBRElFTlQsIEdyYXBoaWNzIH0gZnJvbSBcInBpeGkuanNcIjtcclxuXHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgdmFyIGNvbnNvbGVUZXh0ID0gbmV3IFRleHQoXCJFeGVyY2lzZTEgaW4gQ29uc29sZSBcIiwge2ZpbGw6IFwiMHgwMGZmZmZcIiwgZm9udFNpemU6IDI4LCB9KTtcclxuICAgICAgY29uc29sZVRleHQueCA9IDMwO1xyXG4gICAgICBjb25zb2xlVGV4dC55ID0gMTAwO1xyXG5cclxuICAgICAgY29uc29sZS5sb2coXCJFeGVyY2lzZTFcIik7XHJcbiAgICAgIFxyXG4gICAgICAvLyA9PT09PT09PT09PT09ICMxID09PT09PT09PT09PT1cclxuICAgICAgdmFyIGxhcmdlc3RBcnIgPSBmdW5jdGlvbihpbnQxLCBpbnQyLCBpbnQzLCBpbnQ0LCBsYXJnZXN0RWxlbSlcclxuICAgICAge1xyXG4gICAgICAgICAgdmFyIGFycmF5ID0gW2ludDEsIGludDIsIGludDMsIGludDRdXHJcbiAgICAgICAgICBsYXJnZXN0RWxlbSA9IDA7XHJcblxyXG4gICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGk8PSBsYXJnZXN0RWxlbTsgaSsrKVxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGlmKGFycmF5W2ldID4gbGFyZ2VzdEVsZW0pXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFyZ2VzdEVsZW0gPSBhcnJheVtpXTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKFwiMS4gRmluZCB0aGUgaGlnaGVzdCBudW1iZXN0IGluIGFuIGFycmF5OiBbXCIgKyBhcnJheSArIFwiXSAtIEhpZ2hlc3QgaXMgXCIgKyBsYXJnZXN0RWxlbSk7XHJcblxyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyA9PT09PT09PT09PT09ICMyID09PT09PT09PT09PT1cclxuICAgICAgdmFyIHN1bUFyciA9IGZ1bmN0aW9uKGludDEsIGludDIsIGludDMsIGludDQpIFxyXG4gICAgICB7XHJcbiAgICAgICAgICB2YXIgYXJyYXkgPSBbaW50MSwgaW50MiwgaW50MywgaW50NF07XHJcbiAgICAgICAgICB2YXIgdG90YWwgPSAwO1xyXG5cclxuICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGk8IGFycmF5Lmxlbmd0aDsgaSsrKVxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICAgIHRvdGFsICs9IGFycmF5W2ldO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHJldHVybiBjb25zb2xlLmxvZyhcIjIuIEZpbmQgdGhlIHN1bSBvZiBhbGwgZWxlbWVudCBpbiBhbiBhcnJheTogW1wiICsgYXJyYXkgKyBcIl0sIFN1bSBvZiB0aGUgYXJyYXkgaXMgLSBcIiArIHRvdGFsKVxyXG5cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gPT09PT09PT09PT09PSAjMyA9PT09PT09PT09PT09XHJcbiAgICAgIHZhciBhdmdBcnIgPSBmdW5jdGlvbihpbnQxLCBpbnQyLCBpbnQzLCBpbnQ0LCBpbnQ1LCBpbnQ2KVxyXG4gICAgICB7XHJcbiAgICAgICAgICB2YXIgYXJyYXkgPSBbaW50MSwgaW50MiwgaW50MywgaW50NCwgaW50NSwgaW50Nl1cclxuICAgICAgICAgIHZhciBhdmVyYWdlID0gMDtcclxuXHJcbiAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgYXZlcmFnZSArPSBhcnJheVtpXTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICByZXR1cm4gY29uc29sZS5sb2coXCIzLiBGaW5kIHRoZSBhdmVyYWdlIG9mIGdpdmVuIGVsZW1lbnRzL251bWJlcnM6IFRoZSBhdmVyYWdlIG9mIFwiICsgYXJyYXkgKyBcIiBpcyA9IFwiICsgYXZlcmFnZS9hcnJheS5sZW5ndGgpXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vID09PT09PT09PT09PT0gIzQgPT09PT09PT09PT09PVxyXG4gICAgICB2YXIgcmFkQ29udmVydCA9IGZ1bmN0aW9uKGludDEsIHBpZSwgZGVncmVlKVxyXG4gICAgICB7XHJcbiAgICAgICAgICAgIHBpZSA9IDMuMTQ7XHJcbiAgICAgICAgICAgIGRlZ3JlZSA9IDE4MDtcclxuXHJcbiAgICAgICAgICAgIHZhciByYWRpYW4gPSBpbnQxICogcGllL2RlZ3JlZTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBjb25zb2xlLmxvZyhcIjQuIENvbnZlcnQgYSBudW1iZXIgZnJvbSBkZWdyZWVzIHRvIHJhZGlhbnMuIFwiICsgaW50MSArIFwiIGRlZ3JlZXMgaXMgPSBcIiArIHJhZGlhbiArIFwiIHJhZGlhbnMgXCIpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyA9PT09PT09PT09PT09ICM1ID09PT09PT09PT09PT1cclxuICAgICAgdmFyIGNsYW1waW5nID0gZnVuY3Rpb24oaW50MSwgbWluLCBtYXgpXHJcbiAgICAgIHtcclxuICAgICAgICAgIHZhciB1bmNsYW1wID0gaW50MTtcclxuXHJcbiAgICAgICAgICBpZihpbnQxIDw9IG1pbilcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBpbnQxID0gbWluO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgZWxzZSBpZiAoaW50MSA+PSBtYXgpXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgaW50MSA9IG1heDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICByZXR1cm4gY29uc29sZS5sb2coXCI1LiBBIG51bWJlciB0aGF0IGNsYW1wcyB0byBtaW5pbXVtIGFuZCBtYXhpbXVtLiBVbmNsYW1wZWQgbnVtYmVyIGlzID0gXCIgKyB1bmNsYW1wICsgXCIsIE1pbmltdW0gPSBcIiArIG1pbiArIFxyXG4gICAgICAgICAgIFwiLCBNYXhpbXVtID0gXCIgKyBtYXggKyBcIlxcblwiICsgXCJDbGFtcGVkIG51bWJlciA9IFwiICsgaW50MSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vID09PT09PT09PT09PT0gIzYgPT09PT09PT09PT09PVxyXG4gICAgICB2YXIgZGlzdGFuY2UgPSBmdW5jdGlvbih4MSwgeDIsIHkxLCB5MilcclxuICAgICAge1xyXG4gICAgICAgICAgdmFyIGRpc3RhbmNlID0gTWF0aC5zcXJ0KCh4MiAtIHgxKSooeDIteDEpICsgKHkyIC15MSkgKiAoeTIgLXkxKSk7XHJcblxyXG4gICAgICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKFwiNi4gR2V0IHRoZSBFdWNsaWRlYW4gZGlzdGFuY2Ugb2YgdGhlIHR3byBwb2ludHMuXCIgKyBcIlxcblwiICsgXCJQb2ludCAjMSA9IChcIiArIFxyXG4gICAgICAgICAgeDEgKyBcIiwgXCIgK3kxKyBcIiksIFBvaW50ICMyID0gKFwiICsgeDIgKyBcIiwgXCIgKyB5MiArIFwiKSwgVGhlIGRpc3RhbmNlIGlzID0gXCIgICsgZGlzdGFuY2UpXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vID09PT09PT09PT09PT0gIzcgPT09PT09PT09PT09PVxyXG4gICAgICB2YXIgc29ydGluZyA9IGZ1bmN0aW9uKGludDEsIGludDIsIGludDMsIGludDQsIGludDUpXHJcbiAgICAgIHtcclxuICAgICAgICAgIHZhciBhcnJheSA9IFtpbnQxLCBpbnQyLCBpbnQzLCBpbnQ0LCBpbnQ1XVxyXG4gICAgICAgICAgdmFyIHVuc29ydGVkID0gYXJyYXk7XHJcblxyXG4gICAgICAgICAgZnVuY3Rpb24gc29ydE51bWJlcihhLGIpIFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICAgcmV0dXJuIGEgLSBiO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHJldHVybiBjb25zb2xlLmxvZyhcIjcuIFNvcnRpbmcgb2YgbnVtYmVyIGVsZW1lbnRzIGluIGFuIGFycmF5IChMb3dlc3QgdG8gSGlnaGVzdCkuIFVuc29ydGVkID0gW1wiICsgdW5zb3J0ZWQgKyBcIl0sIFNvcnRlZCA9IFtcIiArIGFycmF5LnNvcnQoc29ydE51bWJlcikgKyBcIl1cIik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vID09PT09PT09PT09PT0gIzggPT09PT09PT09PT09PVxyXG4gICAgICB2YXIgb2JqZWN0MSA9IHtcclxuICAgICAgICAgIF94IDogMCxcclxuICAgICAgICAgIF95IDogMCxcclxuICAgICAgICAgIF9NYWduaXR1ZGUgOiAwXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vID09PT09PT09PT09PT0gIzkgPT09PT09PT09PT09PVxyXG4gICAgICB2YXIgUGVyc29uID0gKGZpcnN0TmFtZSwgbGFzdE5hbWUpPT5cclxuICAgICAge1xyXG4gICAgICAgICAgdGhpcy5maXJzdE5hbWUgPSBmaXJzdE5hbWU7XHJcbiAgICAgICAgICB0aGlzLmxhc3ROYW1lID0gbGFzdE5hbWU7XHJcblxyXG4gICAgICAgICAgdGhpcy5nZXRGdWxsTmFtZSA9IGZ1bmN0aW9uKClcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgICB2YXIgZnVsbE5hbWUgPSB0aGlzLmZpcnN0TmFtZSArIFwiIFwiICsgdGhpcy5sYXN0TmFtZTtcclxuXHJcbiAgICAgICAgICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKFwiOS4gQ3JlYXRlIGEgY2xhc3MgdGhhdCBoYXMgRmlyc3RuYW1lLCBMYXN0IG5hbWUsIGFuZCBoYXMgZ2V0RnVsbE5hbWUgZnVuY3Rpb246IFwiICsgXCJcXG5cIiArXCJGdWxsbmFtZSA6IFwiICsgZnVsbE5hbWUgKyBcIlxcblwiICsgXCJMYXN0bmFtZTogXCIgKyB0aGlzLmxhc3ROYW1lICsgXCJcXG5cIiArIFwiRmlyc3RuYW1lOiBcIiArIHRoaXMuZmlyc3ROYW1lKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5nZXRGdWxsTmFtZSgpO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgdGhpcy5hZGRDaGlsZChjb25zb2xlVGV4dCk7XHJcblxyXG4gICAgICB2YXIgYXJyYXkxID0gbGFyZ2VzdEFycigxMiwgMzIsIDU1LCAyNyk7XHJcbiAgICAgIHZhciBhcnJheTIgPSBzdW1BcnIoNCwgMTAsIDksIDUpO1xyXG4gICAgICB2YXIgYXJyYXkzID0gYXZnQXJyKDUsIDE4LCAxMiwgMTAsIDI1LCAxNSk7XHJcbiAgICAgIHZhciBkZWdUb3JhZCA9IHJhZENvbnZlcnQoOTApO1xyXG4gICAgICB2YXIgY2xhbXAgPSBjbGFtcGluZyg5OTk2OTksIDIwMCwgMzUwKTtcclxuICAgICAgdmFyIGV1Y0Rpc3RhbmNlID0gZGlzdGFuY2UoMiwgLTIsIC0xLCAyKTtcclxuICAgICAgdmFyIHNvcnQgPSBzb3J0aW5nKDUyLCAyNSwgNzksIDEwMiwgNjQpO1xyXG5cclxuICAgICAgdmFyIGNyZWF0ZU9iaiA9IG5ldyBPYmplY3QoKTtcclxuICAgICAgY3JlYXRlT2JqLl94ID0gNTI7XHJcbiAgICAgIGNyZWF0ZU9iai5feSA9IDI1O1xyXG4gICAgICBjcmVhdGVPYmouX01hZ25pdHVkZSA9IGNyZWF0ZU9iai5feCArIGNyZWF0ZU9iai5feTtcclxuXHJcbiAgICAgIGNvbnNvbGUubG9nKFwiOC4gQ3JlYXRlIGFuIG9iamVjdCB3aXRoIHRoZSB4LCB5LCBhbmQgdGhlIGxlbmd0aCBvZiB0aGUgcG9pbnQ6IFwiICsgY3JlYXRlT2JqKTtcclxuXHJcbiAgICAgIHZhciBteXNlbGYgPSBuZXcgUGVyc29uKFwiUnl1IEVsaWphaFwiLCBcIkVscXVpZXJvXCIpO1xyXG4gICAgICBcclxuICAgICAgXHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRpdGxlU2NyZWVuOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7QUFDQTs7Ozs7OztBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFySkE7QUF3SkE7QUFDQTs7QUExSkE7QUFDQTtBQTJKQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });