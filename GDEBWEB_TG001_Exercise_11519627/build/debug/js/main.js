/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/andayani.eot\", \"assets/fonts/andayani.svg\", \"assets/fonts/andayani.ttf\", \"assets/fonts/andayani.woff\", \"assets/fonts/andayani.woff2\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS5lb3RcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS5zdmdcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS50dGZcIixcblx0XHRcImFzc2V0cy9mb250cy9hbmRheWFuaS53b2ZmXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvYW5kYXlhbmkud29mZjJcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtdXG59OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBT0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: 2000,\n    height: 1000\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiAyMDAwLFxyXG4gICAgaGVpZ2h0OiAxMDAwXHJcbn0pXHJcblxyXG5cclxuZG9jdW1lbnQuYm9keS5zdHlsZS5tYXJnaW4gPSBcIjBweFwiO1xyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gXCJoaWRkZW5cIjtcclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKiBFTlRSWSBQT0lOVCAqKioqKioqKioqKioqKipcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5mdW5jdGlvbiByZWFkeShmbikge1xyXG4gICAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT0gJ2xvYWRpbmcnKSB7XHJcbiAgICAgICAgZm4oKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZuKTtcclxuICAgIH1cclxufVxyXG5cclxucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgTWFpbi5zdGFydCgpO1xyXG59KTtcclxuXHJcbmV4cG9ydCB7cGl4aWFwcCBhcyBBcHB9OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n        value: true\n});\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n        _inherits(TitleScreen, _Container);\n\n        function TitleScreen() {\n                _classCallCheck(this, TitleScreen);\n\n                ////////////// +++++++ SHAPES ++++++++ ////////////////\n\n\n                /// ===== #1 Draw a circle filled with color blue with a radius of 45 =====\n\n                var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n                var NM_one_circleFill = new _pixi.Graphics();\n                NM_one_circleFill.beginFill(0x0000ff);\n                NM_one_circleFill.drawCircle(50, 50, 45);\n                NM_one_circleFill.endFill();\n\n                _this.addChild(NM_one_circleFill);\n\n                /// ===== #2 Draw a rectangle filled with color red with width of 150 and height of 210. =====\n\n                var NM_two_rectFill = new _pixi.Graphics();\n                NM_two_rectFill.beginFill(0xff0000);\n                NM_two_rectFill.drawRect(50, 150, 150, 210);\n\n                _this.addChild(NM_two_rectFill);\n\n                /// ===== #3 Draw an ellipse filled with color green with width of 180 and height of 75. =====\n\n                var NM_three_ellipseFill = new _pixi.Graphics();\n                NM_three_ellipseFill.beginFill(0x00ff00);\n                NM_three_ellipseFill.drawEllipse(200, 550, 180, 75);\n\n                _this.addChild(NM_three_ellipseFill);\n\n                /// ===== #4 Draw #1 without any fill. =====\n\n                var NM_four_circleLine = new _pixi.Graphics();\n                NM_four_circleLine.lineStyle(4, 0x0000ff);\n                NM_four_circleLine.drawCircle(50, 750, 45);\n\n                _this.addChild(NM_four_circleLine);\n\n                /// ===== #5 Draw #2 without any fill. =====\n\n                var NM_five_rectLine = new _pixi.Graphics();\n                NM_five_rectLine.lineStyle(4, 0xff0000);\n                NM_five_rectLine.drawRect(50, 900, 150, 210);\n\n                _this.addChild(NM_five_rectLine);\n\n                // ===== #6 Draw #3 without any fill =====\n\n                var NM_six_ellipseLine = new _pixi.Graphics();\n                NM_six_ellipseLine.lineStyle(4, 0x00ff00);\n                NM_six_ellipseLine.drawEllipse(550, 100, 180, 75);\n\n                _this.addChild(NM_six_ellipseLine);\n\n                // ==== #7 Draw box using moveTo and lineTo only =====\n\n                var NM_seven_boxLine = new _pixi.Graphics();\n                NM_seven_boxLine.lineStyle(6, 0x00ffb9);\n\n                //Upper right angle\n                NM_seven_boxLine.moveTo(600, 300);\n                NM_seven_boxLine.lineTo(450, 300);\n                NM_seven_boxLine.lineTo(450, 450);\n\n                //Bottom right angle\n                NM_seven_boxLine.moveTo(450, 450);\n                NM_seven_boxLine.lineTo(600, 450);\n                NM_seven_boxLine.lineTo(600, 300);\n\n                _this.addChild(NM_seven_boxLine);\n\n                // ===== #8 Draw a star. =====\n                var NM_eight_star = new _pixi.Graphics();\n                NM_eight_star.lineStyle(4, 0xb900ff);\n\n                //horizontal mid\n                NM_eight_star.moveTo(525, 600);\n                NM_eight_star.lineTo(625, 600);\n                //diagonal down left\n                NM_eight_star.lineTo(525, 675);\n                //diagonal top left\n                NM_eight_star.lineTo(575, 545);\n                //diagonal bottom right\n                NM_eight_star.lineTo(625, 675);\n                //diagonal mid left to end\n                NM_eight_star.lineTo(525, 600);\n\n                _this.addChild(NM_eight_star);\n\n                // ===== #9 Draw a bunny face =====\n\n                var bunny_Head = new _pixi.Graphics();\n                bunny_Head.beginFill(0xffff00);\n                bunny_Head.drawCircle(475, 900, 70);\n                bunny_Head.endFill();\n\n                var bunny_EarLeft = new _pixi.Graphics();\n                bunny_EarLeft.beginFill(0x00ff00);\n                bunny_EarLeft.drawEllipse(420, 830, 30, 80);\n                bunny_EarLeft.endFill();\n\n                var bunny_EarRight = new _pixi.Graphics();\n                bunny_EarRight.beginFill(0x00ff00);\n                bunny_EarRight.drawEllipse(525, 830, 30, 80);\n                bunny_EarRight.endFill();\n\n                var bunny_Mouth = new _pixi.Graphics();\n                bunny_Mouth.lineStyle(7, 0x0000b9);\n                bunny_Mouth.moveTo(425, 920);\n                bunny_Mouth.lineTo(525, 920);\n\n                var bunny_Eyes = new _pixi.Graphics();\n                bunny_Eyes.beginFill(0xff0000);\n                bunny_Eyes.drawCircle(445, 885, 10);\n                bunny_Eyes.drawCircle(505, 885, 10);\n                bunny_Eyes.endFill();\n\n                var bunny_Teeth = new _pixi.Graphics();\n                bunny_Teeth.lineStyle(3, 0x0000b9);\n                bunny_Teeth.beginFill(0xffffff);\n                bunny_Teeth.drawRect(445, 921, 20, 30);\n                bunny_Teeth.drawRect(485, 921, 20, 30);\n                bunny_Teeth.endFill();\n\n                _this.addChild(bunny_EarLeft);\n                _this.addChild(bunny_EarRight);\n                _this.addChild(bunny_Head);\n                _this.addChild(bunny_Teeth);\n                _this.addChild(bunny_Eyes);\n                _this.addChild(bunny_Mouth);\n\n                ////////////// +++++++ TEXTS ++++++++ ////////////////\n\n                var Texts_PositionsX = 700;\n                var Texts_PositionsY = 500;\n\n                // =====  #1 Display a Text with your ID Number with the ff style: =====\n                var ID_Number = new _pixi.Text(\"ID Number: \" + \"11519627\", { fill: \"0xffffff\", fontSize: 18 });\n                ID_Number.x = Texts_PositionsX;\n                ID_Number.y = Texts_PositionsY;\n\n                // =====  #2 Display a Text with your Name with the ff style:  =====\n                var Name = new _pixi.Text(\"Name: \" + \"Ryu Elijah Elquiero\", { fill: [0x800080, 0x008000, 0x0000ff, 0xffff00], fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_HORIZONTAL, fontSize: 20 });\n                Name.x = Texts_PositionsX;\n                Name.y = Texts_PositionsY + 20;\n\n                // ===== #3 Display a Text with Full Program Name with the ff style: =====\n                var Full_Program = new _pixi.Text(\"Program: \" + \"Game Development for the Web\", { fontSize: 18, fill: [0x00ffb9, 0x0000b9, 0xffff00, 0xb900b9],\n                        fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_VERTICAL, fillGradientStops: [1] });\n                Full_Program.x = Texts_PositionsX;\n                Full_Program.y = Name.y + 20;\n\n                // ===== #4 Display a Text with Full Course Name with the ff style: =====\n                var Full_Course = new _pixi.Text(\"Course: \" + \"Bachelor of Science in Interactive Entertainment and Multimedia Computing\", { fill: \"0x00ff00\", fontSize: 18, wordWrap: true, align: \"center\" });\n                Full_Course.x = Texts_PositionsX;\n                Full_Course.y = Full_Program.y + 20;\n\n                // ===== #5 Display any Text using a custom font =====\n                var Custom_Font = new _pixi.Text(\"Hello EARTH\", { fill: [0x00ffff, 0x4b0082], fontSize: 50, fontFamily: 'Andayani', fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_HORIZONTAL });\n                Custom_Font.x = Texts_PositionsX;\n                Custom_Font.y = Full_Course.y + 200;\n\n                _this.addChild(ID_Number);\n                _this.addChild(Name);\n                _this.addChild(Full_Program);\n                _this.addChild(Full_Course);\n                _this.addChild(Custom_Font);\n\n                return _this;\n        }\n\n        return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsIFRFWFRfR1JBRElFTlQsIEdyYXBoaWNzIH0gZnJvbSBcInBpeGkuanNcIjtcclxuXHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuXHJcbiAgICAgICAgLy8vLy8vLy8vLy8vLy8gKysrKysrKyBTSEFQRVMgKysrKysrKysgLy8vLy8vLy8vLy8vLy8vL1xyXG5cclxuXHJcbiAgICAgICAgLy8vID09PT09ICMxIERyYXcgYSBjaXJjbGUgZmlsbGVkIHdpdGggY29sb3IgYmx1ZSB3aXRoIGEgcmFkaXVzIG9mIDQ1ID09PT09XHJcblxyXG4gICAgICAgIHZhciBOTV9vbmVfY2lyY2xlRmlsbCA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIE5NX29uZV9jaXJjbGVGaWxsLmJlZ2luRmlsbCgweDAwMDBmZik7XHJcbiAgICAgICAgTk1fb25lX2NpcmNsZUZpbGwuZHJhd0NpcmNsZSg1MCwgNTAsIDQ1KTtcclxuICAgICAgICBOTV9vbmVfY2lyY2xlRmlsbC5lbmRGaWxsKCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoTk1fb25lX2NpcmNsZUZpbGwpO1xyXG5cclxuXHJcblxyXG4gICAgICAgIC8vLyA9PT09PSAjMiBEcmF3IGEgcmVjdGFuZ2xlIGZpbGxlZCB3aXRoIGNvbG9yIHJlZCB3aXRoIHdpZHRoIG9mIDE1MCBhbmQgaGVpZ2h0IG9mIDIxMC4gPT09PT1cclxuXHJcbiAgICAgICAgdmFyIE5NX3R3b19yZWN0RmlsbCA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIE5NX3R3b19yZWN0RmlsbC5iZWdpbkZpbGwoMHhmZjAwMDApO1xyXG4gICAgICAgIE5NX3R3b19yZWN0RmlsbC5kcmF3UmVjdCg1MCwgMTUwLCAxNTAsIDIxMCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoTk1fdHdvX3JlY3RGaWxsKTtcclxuXHJcblxyXG5cclxuICAgICAgICAvLy8gPT09PT0gIzMgRHJhdyBhbiBlbGxpcHNlIGZpbGxlZCB3aXRoIGNvbG9yIGdyZWVuIHdpdGggd2lkdGggb2YgMTgwIGFuZCBoZWlnaHQgb2YgNzUuID09PT09XHJcblxyXG4gICAgICAgIHZhciBOTV90aHJlZV9lbGxpcHNlRmlsbCA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIE5NX3RocmVlX2VsbGlwc2VGaWxsLmJlZ2luRmlsbCgweDAwZmYwMCk7XHJcbiAgICAgICAgTk1fdGhyZWVfZWxsaXBzZUZpbGwuZHJhd0VsbGlwc2UoMjAwLCA1NTAsIDE4MCwgNzUpO1xyXG5cclxuICAgICAgICB0aGlzLmFkZENoaWxkKE5NX3RocmVlX2VsbGlwc2VGaWxsKTtcclxuXHJcblxyXG5cclxuICAgICAgICAvLy8gPT09PT0gIzQgRHJhdyAjMSB3aXRob3V0IGFueSBmaWxsLiA9PT09PVxyXG5cclxuICAgICAgICB2YXIgTk1fZm91cl9jaXJjbGVMaW5lID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgTk1fZm91cl9jaXJjbGVMaW5lLmxpbmVTdHlsZSg0LCAweDAwMDBmZik7XHJcbiAgICAgICAgTk1fZm91cl9jaXJjbGVMaW5lLmRyYXdDaXJjbGUoNTAsIDc1MCwgNDUpO1xyXG5cclxuICAgICAgICB0aGlzLmFkZENoaWxkKE5NX2ZvdXJfY2lyY2xlTGluZSk7XHJcblxyXG5cclxuXHJcbiAgICAgICAgLy8vID09PT09ICM1IERyYXcgIzIgd2l0aG91dCBhbnkgZmlsbC4gPT09PT1cclxuXHJcbiAgICAgICAgdmFyIE5NX2ZpdmVfcmVjdExpbmUgPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICBOTV9maXZlX3JlY3RMaW5lLmxpbmVTdHlsZSg0LCAweGZmMDAwMCk7XHJcbiAgICAgICAgTk1fZml2ZV9yZWN0TGluZS5kcmF3UmVjdCg1MCwgOTAwLCAxNTAsIDIxMCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoTk1fZml2ZV9yZWN0TGluZSk7XHJcblxyXG5cclxuXHJcbiAgICAgICAgLy8gPT09PT0gIzYgRHJhdyAjMyB3aXRob3V0IGFueSBmaWxsID09PT09XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIE5NX3NpeF9lbGxpcHNlTGluZSA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIE5NX3NpeF9lbGxpcHNlTGluZS5saW5lU3R5bGUoNCwweDAwZmYwMCk7XHJcbiAgICAgICAgTk1fc2l4X2VsbGlwc2VMaW5lLmRyYXdFbGxpcHNlKDU1MCwgMTAwLCAxODAsIDc1KTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChOTV9zaXhfZWxsaXBzZUxpbmUpO1xyXG5cclxuXHJcblxyXG4gICAgICAgIC8vID09PT0gIzcgRHJhdyBib3ggdXNpbmcgbW92ZVRvIGFuZCBsaW5lVG8gb25seSA9PT09PVxyXG5cclxuICAgICAgICB2YXIgTk1fc2V2ZW5fYm94TGluZSA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIE5NX3NldmVuX2JveExpbmUubGluZVN0eWxlKDYsIDB4MDBmZmI5KTtcclxuXHJcbiAgICAgICAgLy9VcHBlciByaWdodCBhbmdsZVxyXG4gICAgICAgIE5NX3NldmVuX2JveExpbmUubW92ZVRvKDYwMCwgMzAwKTtcclxuICAgICAgICBOTV9zZXZlbl9ib3hMaW5lLmxpbmVUbyg0NTAsIDMwMCk7XHJcbiAgICAgICAgTk1fc2V2ZW5fYm94TGluZS5saW5lVG8oNDUwLCA0NTApXHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9Cb3R0b20gcmlnaHQgYW5nbGVcclxuICAgICAgICBOTV9zZXZlbl9ib3hMaW5lLm1vdmVUbyg0NTAsIDQ1MCk7XHJcbiAgICAgICAgTk1fc2V2ZW5fYm94TGluZS5saW5lVG8oNjAwLCA0NTApO1xyXG4gICAgICAgIE5NX3NldmVuX2JveExpbmUubGluZVRvKDYwMCwgMzAwKTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChOTV9zZXZlbl9ib3hMaW5lKTtcclxuXHJcbiAgICAgICAgXHJcblxyXG4gICAgICAgIC8vID09PT09ICM4IERyYXcgYSBzdGFyLiA9PT09PVxyXG4gICAgICAgIHZhciBOTV9laWdodF9zdGFyID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgTk1fZWlnaHRfc3Rhci5saW5lU3R5bGUoNCwgMHhiOTAwZmYpO1xyXG5cclxuICAgICAgICAvL2hvcml6b250YWwgbWlkXHJcbiAgICAgICAgTk1fZWlnaHRfc3Rhci5tb3ZlVG8oNTI1LCA2MDApO1xyXG4gICAgICAgIE5NX2VpZ2h0X3N0YXIubGluZVRvKDYyNSwgNjAwKTtcclxuICAgICAgICAvL2RpYWdvbmFsIGRvd24gbGVmdFxyXG4gICAgICAgIE5NX2VpZ2h0X3N0YXIubGluZVRvKDUyNSwgNjc1KTtcclxuICAgICAgICAvL2RpYWdvbmFsIHRvcCBsZWZ0XHJcbiAgICAgICAgTk1fZWlnaHRfc3Rhci5saW5lVG8oNTc1LCA1NDUpO1xyXG4gICAgICAgIC8vZGlhZ29uYWwgYm90dG9tIHJpZ2h0XHJcbiAgICAgICAgTk1fZWlnaHRfc3Rhci5saW5lVG8oNjI1LCA2NzUpO1xyXG4gICAgICAgIC8vZGlhZ29uYWwgbWlkIGxlZnQgdG8gZW5kXHJcbiAgICAgICAgTk1fZWlnaHRfc3Rhci5saW5lVG8oNTI1LCA2MDApO1xyXG5cclxuICAgICAgICB0aGlzLmFkZENoaWxkKE5NX2VpZ2h0X3N0YXIpO1xyXG5cclxuXHJcblxyXG4gICAgICAgIC8vID09PT09ICM5IERyYXcgYSBidW5ueSBmYWNlID09PT09XHJcblxyXG4gICAgICAgIHZhciBidW5ueV9IZWFkID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgYnVubnlfSGVhZC5iZWdpbkZpbGwoMHhmZmZmMDApO1xyXG4gICAgICAgIGJ1bm55X0hlYWQuZHJhd0NpcmNsZSg0NzUsIDkwMCwgNzApO1xyXG4gICAgICAgIGJ1bm55X0hlYWQuZW5kRmlsbCgpO1xyXG5cclxuICAgICAgICB2YXIgYnVubnlfRWFyTGVmdCA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIGJ1bm55X0VhckxlZnQuYmVnaW5GaWxsKDB4MDBmZjAwKTtcclxuICAgICAgICBidW5ueV9FYXJMZWZ0LmRyYXdFbGxpcHNlKDQyMCwgODMwLCAzMCwgODApO1xyXG4gICAgICAgIGJ1bm55X0VhckxlZnQuZW5kRmlsbCgpO1xyXG5cclxuICAgICAgICB2YXIgYnVubnlfRWFyUmlnaHQgPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICBidW5ueV9FYXJSaWdodC5iZWdpbkZpbGwoMHgwMGZmMDApO1xyXG4gICAgICAgIGJ1bm55X0VhclJpZ2h0LmRyYXdFbGxpcHNlKDUyNSwgODMwLCAzMCwgODApO1xyXG4gICAgICAgIGJ1bm55X0VhclJpZ2h0LmVuZEZpbGwoKTtcclxuXHJcbiAgICAgICAgdmFyIGJ1bm55X01vdXRoID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgYnVubnlfTW91dGgubGluZVN0eWxlKDcsIDB4MDAwMGI5KTtcclxuICAgICAgICBidW5ueV9Nb3V0aC5tb3ZlVG8oNDI1LCA5MjApO1xyXG4gICAgICAgIGJ1bm55X01vdXRoLmxpbmVUbyg1MjUsIDkyMCk7XHJcblxyXG4gICAgICAgIHZhciBidW5ueV9FeWVzID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgYnVubnlfRXllcy5iZWdpbkZpbGwoMHhmZjAwMDApO1xyXG4gICAgICAgIGJ1bm55X0V5ZXMuZHJhd0NpcmNsZSg0NDUsIDg4NSwgMTApO1xyXG4gICAgICAgIGJ1bm55X0V5ZXMuZHJhd0NpcmNsZSg1MDUsIDg4NSwgMTApO1xyXG4gICAgICAgIGJ1bm55X0V5ZXMuZW5kRmlsbCgpO1xyXG5cclxuICAgICAgICB2YXIgYnVubnlfVGVldGggPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICBidW5ueV9UZWV0aC5saW5lU3R5bGUoMywgMHgwMDAwYjkpO1xyXG4gICAgICAgIGJ1bm55X1RlZXRoLmJlZ2luRmlsbCgweGZmZmZmZik7XHJcbiAgICAgICAgYnVubnlfVGVldGguZHJhd1JlY3QoNDQ1LCA5MjEsIDIwLCAzMCk7XHJcbiAgICAgICAgYnVubnlfVGVldGguZHJhd1JlY3QoNDg1LCA5MjEsIDIwLCAzMCk7XHJcbiAgICAgICAgYnVubnlfVGVldGguZW5kRmlsbCgpO1xyXG5cclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChidW5ueV9FYXJMZWZ0KTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKGJ1bm55X0VhclJpZ2h0KTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKGJ1bm55X0hlYWQpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoYnVubnlfVGVldGgpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoYnVubnlfRXllcyk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChidW5ueV9Nb3V0aCk7XHJcblxyXG5cclxuXHJcbiAgICAgICAvLy8vLy8vLy8vLy8vLyArKysrKysrIFRFWFRTICsrKysrKysrIC8vLy8vLy8vLy8vLy8vLy9cclxuXHJcbiAgICAgICB2YXIgVGV4dHNfUG9zaXRpb25zWCA9IDcwMDtcclxuICAgICAgIHZhciBUZXh0c19Qb3NpdGlvbnNZID0gNTAwO1xyXG5cclxuICAgICAgIC8vID09PT09ICAjMSBEaXNwbGF5IGEgVGV4dCB3aXRoIHlvdXIgSUQgTnVtYmVyIHdpdGggdGhlIGZmIHN0eWxlOiA9PT09PVxyXG4gICAgICAgdmFyIElEX051bWJlciA9IG5ldyBUZXh0KFwiSUQgTnVtYmVyOiBcIiArIFwiMTE1MTk2MjdcIiwge2ZpbGw6IFwiMHhmZmZmZmZcIiwgZm9udFNpemU6IDE4LCB9KTtcclxuICAgICAgIElEX051bWJlci54ID0gVGV4dHNfUG9zaXRpb25zWDtcclxuICAgICAgIElEX051bWJlci55ID0gVGV4dHNfUG9zaXRpb25zWTtcclxuXHJcblxyXG4gICAgICAgLy8gPT09PT0gICMyIERpc3BsYXkgYSBUZXh0IHdpdGggeW91ciBOYW1lIHdpdGggdGhlIGZmIHN0eWxlOiAgPT09PT1cclxuICAgICAgIHZhciBOYW1lID0gbmV3IFRleHQoXCJOYW1lOiBcIiArIFwiUnl1IEVsaWphaCBFbHF1aWVyb1wiLHtmaWxsOiBbMHg4MDAwODAsIDB4MDA4MDAwLCAweDAwMDBmZiwgMHhmZmZmMDBdLCBmaWxsR3JhZGllbnRUeXBlOiBURVhUX0dSQURJRU5ULkxJTkVBUl9IT1JJWk9OVEFMLCBmb250U2l6ZTogMjB9KTtcclxuICAgICAgIE5hbWUueCA9IFRleHRzX1Bvc2l0aW9uc1g7XHJcbiAgICAgICBOYW1lLnkgPSBUZXh0c19Qb3NpdGlvbnNZICsgMjA7XHJcblxyXG5cclxuICAgICAgIC8vID09PT09ICMzIERpc3BsYXkgYSBUZXh0IHdpdGggRnVsbCBQcm9ncmFtIE5hbWUgd2l0aCB0aGUgZmYgc3R5bGU6ID09PT09XHJcbiAgICAgICB2YXIgRnVsbF9Qcm9ncmFtID0gbmV3IFRleHQoXCJQcm9ncmFtOiBcIiArIFwiR2FtZSBEZXZlbG9wbWVudCBmb3IgdGhlIFdlYlwiLCB7Zm9udFNpemU6IDE4LCBmaWxsOiBbMHgwMGZmYjksIDB4MDAwMGI5LCAweGZmZmYwMCwgMHhiOTAwYjldLCBcclxuICAgICAgICBmaWxsR3JhZGllbnRUeXBlOiBURVhUX0dSQURJRU5ULkxJTkVBUl9WRVJUSUNBTCwgZmlsbEdyYWRpZW50U3RvcHM6IFsxXX0pO1xyXG4gICAgICAgRnVsbF9Qcm9ncmFtLnggPSBUZXh0c19Qb3NpdGlvbnNYO1xyXG4gICAgICAgRnVsbF9Qcm9ncmFtLnkgPSBOYW1lLnkgKyAyMDtcclxuICAgICAgIFxyXG5cclxuICAgICAgIC8vID09PT09ICM0IERpc3BsYXkgYSBUZXh0IHdpdGggRnVsbCBDb3Vyc2UgTmFtZSB3aXRoIHRoZSBmZiBzdHlsZTogPT09PT1cclxuICAgICAgIHZhciBGdWxsX0NvdXJzZSA9IG5ldyBUZXh0KFwiQ291cnNlOiBcIiArIFwiQmFjaGVsb3Igb2YgU2NpZW5jZSBpbiBJbnRlcmFjdGl2ZSBFbnRlcnRhaW5tZW50IGFuZCBNdWx0aW1lZGlhIENvbXB1dGluZ1wiLCB7ZmlsbDogXCIweDAwZmYwMFwiLCBmb250U2l6ZTogMTgsIHdvcmRXcmFwOiB0cnVlLCBhbGlnbjogXCJjZW50ZXJcIn0pO1xyXG4gICAgICAgRnVsbF9Db3Vyc2UueCA9IFRleHRzX1Bvc2l0aW9uc1g7XHJcbiAgICAgICBGdWxsX0NvdXJzZS55ID0gRnVsbF9Qcm9ncmFtLnkrMjA7XHJcblxyXG5cclxuICAgICAgIC8vID09PT09ICM1IERpc3BsYXkgYW55IFRleHQgdXNpbmcgYSBjdXN0b20gZm9udCA9PT09PVxyXG4gICAgICAgdmFyIEN1c3RvbV9Gb250ID0gbmV3IFRleHQoXCJIZWxsbyBFQVJUSFwiLCB7ZmlsbDogWzB4MDBmZmZmLCAweDRiMDA4Ml0sIGZvbnRTaXplOiA1MCwgZm9udEZhbWlseTogJ0FuZGF5YW5pJyxmaWxsR3JhZGllbnRUeXBlOiBURVhUX0dSQURJRU5ULkxJTkVBUl9IT1JJWk9OVEFMfSk7XHJcbiAgICAgICBDdXN0b21fRm9udC54ID0gVGV4dHNfUG9zaXRpb25zWDtcclxuICAgICAgIEN1c3RvbV9Gb250LnkgPSBGdWxsX0NvdXJzZS55ICsgMjAwO1xyXG5cclxuXHJcbiAgICAgICB0aGlzLmFkZENoaWxkKElEX051bWJlcik7XHJcbiAgICAgICB0aGlzLmFkZENoaWxkKE5hbWUpO1xyXG4gICAgICAgdGhpcy5hZGRDaGlsZChGdWxsX1Byb2dyYW0pO1xyXG4gICAgICAgdGhpcy5hZGRDaGlsZChGdWxsX0NvdXJzZSk7XHJcbiAgICAgICB0aGlzLmFkZENoaWxkKEN1c3RvbV9Gb250KTtcclxuICAgICAgICBcclxuICAgICAgICBcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVGl0bGVTY3JlZW47Il0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpNQTtBQW1NQTtBQUNBOztBQXJNQTtBQUNBO0FBc01BIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });