import { Container, Text, TEXT_GRADIENT, Graphics } from "pixi.js";

class TitleScreen extends Container{
    constructor(){
        super();


        ////////////// +++++++ SHAPES ++++++++ ////////////////


        /// ===== #1 Draw a circle filled with color blue with a radius of 45 =====

        var NM_one_circleFill = new Graphics();
        NM_one_circleFill.beginFill(0x0000ff);
        NM_one_circleFill.drawCircle(50, 50, 45);
        NM_one_circleFill.endFill();

        this.addChild(NM_one_circleFill);



        /// ===== #2 Draw a rectangle filled with color red with width of 150 and height of 210. =====

        var NM_two_rectFill = new Graphics();
        NM_two_rectFill.beginFill(0xff0000);
        NM_two_rectFill.drawRect(50, 150, 150, 210);

        this.addChild(NM_two_rectFill);



        /// ===== #3 Draw an ellipse filled with color green with width of 180 and height of 75. =====

        var NM_three_ellipseFill = new Graphics();
        NM_three_ellipseFill.beginFill(0x00ff00);
        NM_three_ellipseFill.drawEllipse(200, 550, 180, 75);

        this.addChild(NM_three_ellipseFill);



        /// ===== #4 Draw #1 without any fill. =====

        var NM_four_circleLine = new Graphics();
        NM_four_circleLine.lineStyle(4, 0x0000ff);
        NM_four_circleLine.drawCircle(50, 750, 45);

        this.addChild(NM_four_circleLine);



        /// ===== #5 Draw #2 without any fill. =====

        var NM_five_rectLine = new Graphics();
        NM_five_rectLine.lineStyle(4, 0xff0000);
        NM_five_rectLine.drawRect(50, 900, 150, 210);

        this.addChild(NM_five_rectLine);



        // ===== #6 Draw #3 without any fill =====
        
        var NM_six_ellipseLine = new Graphics();
        NM_six_ellipseLine.lineStyle(4,0x00ff00);
        NM_six_ellipseLine.drawEllipse(550, 100, 180, 75);

        this.addChild(NM_six_ellipseLine);



        // ==== #7 Draw box using moveTo and lineTo only =====

        var NM_seven_boxLine = new Graphics();
        NM_seven_boxLine.lineStyle(6, 0x00ffb9);

        //Upper right angle
        NM_seven_boxLine.moveTo(600, 300);
        NM_seven_boxLine.lineTo(450, 300);
        NM_seven_boxLine.lineTo(450, 450)
        
        //Bottom right angle
        NM_seven_boxLine.moveTo(450, 450);
        NM_seven_boxLine.lineTo(600, 450);
        NM_seven_boxLine.lineTo(600, 300);

        this.addChild(NM_seven_boxLine);

        

        // ===== #8 Draw a star. =====
        var NM_eight_star = new Graphics();
        NM_eight_star.lineStyle(4, 0xb900ff);

        //horizontal mid
        NM_eight_star.moveTo(525, 600);
        NM_eight_star.lineTo(625, 600);
        //diagonal down left
        NM_eight_star.lineTo(525, 675);
        //diagonal top left
        NM_eight_star.lineTo(575, 545);
        //diagonal bottom right
        NM_eight_star.lineTo(625, 675);
        //diagonal mid left to end
        NM_eight_star.lineTo(525, 600);

        this.addChild(NM_eight_star);



        // ===== #9 Draw a bunny face =====

        var bunny_Head = new Graphics();
        bunny_Head.beginFill(0xffff00);
        bunny_Head.drawCircle(475, 900, 70);
        bunny_Head.endFill();

        var bunny_EarLeft = new Graphics();
        bunny_EarLeft.beginFill(0x00ff00);
        bunny_EarLeft.drawEllipse(420, 830, 30, 80);
        bunny_EarLeft.endFill();

        var bunny_EarRight = new Graphics();
        bunny_EarRight.beginFill(0x00ff00);
        bunny_EarRight.drawEllipse(525, 830, 30, 80);
        bunny_EarRight.endFill();

        var bunny_Mouth = new Graphics();
        bunny_Mouth.lineStyle(7, 0x0000b9);
        bunny_Mouth.moveTo(425, 920);
        bunny_Mouth.lineTo(525, 920);

        var bunny_Eyes = new Graphics();
        bunny_Eyes.beginFill(0xff0000);
        bunny_Eyes.drawCircle(445, 885, 10);
        bunny_Eyes.drawCircle(505, 885, 10);
        bunny_Eyes.endFill();

        var bunny_Teeth = new Graphics();
        bunny_Teeth.lineStyle(3, 0x0000b9);
        bunny_Teeth.beginFill(0xffffff);
        bunny_Teeth.drawRect(445, 921, 20, 30);
        bunny_Teeth.drawRect(485, 921, 20, 30);
        bunny_Teeth.endFill();


        this.addChild(bunny_EarLeft);
        this.addChild(bunny_EarRight);
        this.addChild(bunny_Head);
        this.addChild(bunny_Teeth);
        this.addChild(bunny_Eyes);
        this.addChild(bunny_Mouth);



       ////////////// +++++++ TEXTS ++++++++ ////////////////

       var Texts_PositionsX = 700;
       var Texts_PositionsY = 500;

       // =====  #1 Display a Text with your ID Number with the ff style: =====
       var ID_Number = new Text("ID Number: " + "11519627", {fill: "0xffffff", fontSize: 18, });
       ID_Number.x = Texts_PositionsX;
       ID_Number.y = Texts_PositionsY;


       // =====  #2 Display a Text with your Name with the ff style:  =====
       var Name = new Text("Name: " + "Ryu Elijah Elquiero",{fill: [0x800080, 0x008000, 0x0000ff, 0xffff00], fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL, fontSize: 20});
       Name.x = Texts_PositionsX;
       Name.y = Texts_PositionsY + 20;


       // ===== #3 Display a Text with Full Program Name with the ff style: =====
       var Full_Program = new Text("Program: " + "Game Development for the Web", {fontSize: 18, fill: [0x00ffb9, 0x0000b9, 0xffff00, 0xb900b9], 
        fillGradientType: TEXT_GRADIENT.LINEAR_VERTICAL, fillGradientStops: [1]});
       Full_Program.x = Texts_PositionsX;
       Full_Program.y = Name.y + 20;
       

       // ===== #4 Display a Text with Full Course Name with the ff style: =====
       var Full_Course = new Text("Course: " + "Bachelor of Science in Interactive Entertainment and Multimedia Computing", {fill: "0x00ff00", fontSize: 18, wordWrap: true, align: "center"});
       Full_Course.x = Texts_PositionsX;
       Full_Course.y = Full_Program.y+20;


       // ===== #5 Display any Text using a custom font =====
       var Custom_Font = new Text("Hello EARTH", {fill: [0x00ffff, 0x4b0082], fontSize: 50, fontFamily: 'Andayani',fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL});
       Custom_Font.x = Texts_PositionsX;
       Custom_Font.y = Full_Course.y + 200;


       this.addChild(ID_Number);
       this.addChild(Name);
       this.addChild(Full_Program);
       this.addChild(Full_Course);
       this.addChild(Custom_Font);
        
        
    }
}

export default TitleScreen;